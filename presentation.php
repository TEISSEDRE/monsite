<?php
require 'inc/header.php';
?>

<div class="presentation-image">

    <p class="prenom-nom"> Je suis Robin Teissedre </p>
    <p class="portfolio"> Et voici mon portfolio </p>

</div>

<div class="grand-div-presentation">
    <h1 class="qui-suis"> Qui suis-je ? </h1>

    <div class="div-presentation">
        
        <div class="div-text-presentation">
            
            <p class="text-presentation"> Je suis Robin Teissedre , j'étudie actuellement en BTS SIO (Services Informatiques aux Organisations) option SLAM (Solutions Logicielles et Application Métier) à l'Itescia à Pontoise dans le 95. </p>
        
        </div>
        
        <div>

            <img class="photo-moi" src="images/moi.png" alt="Photo de Robin Teissedre">

        </div>
    </div>
    
    <a href="https://www.itescia.fr/" >
    <img class="logo-itescia"  src="images/itescia.png" alt="Logo de l'Itescia" width=250>
    </a>
</div>
    
<div class="musculation">

    <div class="display-muscu">
        
        <div class="div-video-muscu">
            
            <p class="legende-video-muscu"> Lancer la vidéo  </p>
            <video id="video-muscu" muted  src="vidéo/pompes.mp4"> Vidéo de pompes </video>
            <div class="wrapper" >
            <a class="cta" >
            <span style="color:white">PLAY</span>
            <span>
            <svg width="66px" height="43px" viewBox="0 0 66 43" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g id="arrow" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <path class="one" d="M40.1543933,3.89485454 L43.9763149,0.139296592 C44.1708311,-0.0518420739 44.4826329,-0.0518571125 44.6771675,0.139262789 L65.6916134,20.7848311 C66.0855801,21.1718824 66.0911863,21.8050225 65.704135,22.1989893 C65.7000188,22.2031791 65.6958657,22.2073326 65.6916762,22.2114492 L44.677098,42.8607841 C44.4825957,43.0519059 44.1708242,43.0519358 43.9762853,42.8608513 L40.1545186,39.1069479 C39.9575152,38.9134427 39.9546793,38.5968729 40.1481845,38.3998695 C40.1502893,38.3977268 40.1524132,38.395603 40.1545562,38.3934985 L56.9937789,21.8567812 C57.1908028,21.6632968 57.193672,21.3467273 57.0001876,21.1497035 C56.9980647,21.1475418 56.9959223,21.1453995 56.9937605,21.1432767 L40.1545208,4.60825197 C39.9574869,4.41477773 39.9546013,4.09820839 40.1480756,3.90117456 C40.1501626,3.89904911 40.1522686,3.89694235 40.1543933,3.89485454 Z" fill="#FFFFFF"></path>
          <path class="two" d="M20.1543933,3.89485454 L23.9763149,0.139296592 C24.1708311,-0.0518420739 24.4826329,-0.0518571125 24.6771675,0.139262789 L45.6916134,20.7848311 C46.0855801,21.1718824 46.0911863,21.8050225 45.704135,22.1989893 C45.7000188,22.2031791 45.6958657,22.2073326 45.6916762,22.2114492 L24.677098,42.8607841 C24.4825957,43.0519059 24.1708242,43.0519358 23.9762853,42.8608513 L20.1545186,39.1069479 C19.9575152,38.9134427 19.9546793,38.5968729 20.1481845,38.3998695 C20.1502893,38.3977268 20.1524132,38.395603 20.1545562,38.3934985 L36.9937789,21.8567812 C37.1908028,21.6632968 37.193672,21.3467273 37.0001876,21.1497035 C36.9980647,21.1475418 36.9959223,21.1453995 36.9937605,21.1432767 L20.1545208,4.60825197 C19.9574869,4.41477773 19.9546013,4.09820839 20.1480756,3.90117456 C20.1501626,3.89904911 20.1522686,3.89694235 20.1543933,3.89485454 Z" fill="#FFFFFF"></path>
          <path class="three" d="M0.154393339,3.89485454 L3.97631488,0.139296592 C4.17083111,-0.0518420739 4.48263286,-0.0518571125 4.67716753,0.139262789 L25.6916134,20.7848311 C26.0855801,21.1718824 26.0911863,21.8050225 25.704135,22.1989893 C25.7000188,22.2031791 25.6958657,22.2073326 25.6916762,22.2114492 L4.67709797,42.8607841 C4.48259567,43.0519059 4.17082418,43.0519358 3.97628526,42.8608513 L0.154518591,39.1069479 C-0.0424848215,38.9134427 -0.0453206733,38.5968729 0.148184538,38.3998695 C0.150289256,38.3977268 0.152413239,38.395603 0.154556228,38.3934985 L16.9937789,21.8567812 C17.1908028,21.6632968 17.193672,21.3467273 17.0001876,21.1497035 C16.9980647,21.1475418 16.9959223,21.1453995 16.9937605,21.1432767 L0.15452076,4.60825197 C-0.0425130651,4.41477773 -0.0453986756,4.09820839 0.148075568,3.90117456 C0.150162624,3.89904911 0.152268631,3.89694235 0.154393339,3.89485454 Z" fill="#FFFFFF"></path>
          </g>
          </svg>
          </span> 
          </a>
        </div>

        </div>
            
        <div>

            <p class="text-muscu-1"> Je suis passionné par la musculation depuis que j'ai 13 ans </p>
            <p class="text-muscu-2"> cela fait 10 ans que je pratique et j'ai tout appris </p>
            <p class="text-muscu-3"> en auto didacte , en lisant des livres et en pratiquant. </p>
            <img src="images/bonhomme-muscu.gif" class="bonhomme-muscu" alt="Gif de goku"></img>
        
        </div>

        <script>
            /* Pour faire apparaitre le text de la vidéo de muscu et le gif et faire disparaitre le "Lancer la vidéo" */
            $(document).ready(function() {
            $('.wrapper').click(function() {
            $('.text-muscu-1').css("display","block")
            $('.text-muscu-2').css("display","block")
            $('.text-muscu-3').css("display","block")
            $('.bonhomme-muscu').css("display","block")
            $('.legende-video-muscu').css("visibility","hidden")
            $('#video-muscu').get(0).play();
            });
            });

        </script>

    </div>
</div>


    <h3 class="differents-langage"> Les différents langages de programmation que je pratique et la maitrise que j'en ai </h3>

    
        
        <div class="barre-progression-1"> 
        <p class="titre-barre-progression"> Html </p>
            <div class="couleur-progression">
            <p class="pourcentage-html"> 100 % </p>
            </div>
        
        </div>
        
            <script>

            $('.couleur-progression').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 1800) 
                {
                    $('.couleur-progression').css("display","block")
                    $('.pourcentage-html').css("display","block")
                } 
            });
            });
            </script>
        
        
    
        <div class="barre-progression-2"> 
        <p class="titre-barre-progression2"> CSS </p>
            <div class="couleur-progression2">
            <p class="pourcentage-css"> 80 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression2').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 1900) 
                {
                    $('.couleur-progression2').css("display","block")
                    $('.pourcentage-css').css("display","block")
                } 
            });
            });
            </script>
    
        <div class="barre-progression-3"> 
        <p class="titre-barre-progression3"> Virtual Basic </p>
            <div class="couleur-progression3">
            <p class="pourcentage-virtualbasic"> 60 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression3').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 2050) 
                {
                    $('.couleur-progression3').css("display","block")
                    $('.pourcentage-virtualbasic').css("display","block")
                } 
            });
            });
        </script>

        <div class="barre-progression-4"> 
        <p class="titre-barre-progression4"> C# </p>
            <div class="couleur-progression4">
            <p class="pourcentage-C"> 60 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression4').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 2130) 
                {
                    $('.couleur-progression4').css("display","block")
                    $('.pourcentage-C').css("display","block")
                } 
            });
            });
        </script>

        <div class="barre-progression-5"> 
        <p class="titre-barre-progression5"> Php </p>
            <div class="couleur-progression5">
            <p class="pourcentage-php"> 50 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression5').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 2300) 
                {
                    $('.couleur-progression5').css("display","block")
                    $('.pourcentage-php').css("display","block")
                } 
            });
            });
        </script>
        
        <div class="barre-progression-6"> 
        <p class="titre-barre-progression6"> Java Script </p>
            <div class="couleur-progression6">
            <p class="pourcentage-js"> 50 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression6').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 2400) 
                {
                    $('.couleur-progression6').css("display","block")
                    $('.pourcentage-js').css("display","block")
                } 
            });
            });
        </script>


        <h3 class="titre-frameworks"> Et les frameworks </h3>

        <div class="barre-progression-7"> 
        <p class="titre-barre-progression7"> Bootstrap </p>
            <div class="couleur-progression7">
            <p class="pourcentage-bootstrap"> 70 % </p>
            </div>
        
        </div>

        <script>

            $('.couleur-progression7').css("display","none");

            var $document = $( document );
            
            
            $document.ready( function () {

                $(window).scroll(
                function() {
                if ($(window).scrollTop() > 2600) 
                {
                    $('.couleur-progression7').css("display","block")
                    $('.pourcentage-bootstrap').css("display","block")
                } 
            });
            });
        </script>

        <div class="barre-progression-8"> 
        <p class="titre-barre-progression8"> jQuery </p>
            <div class="couleur-progression8">
            <p class="pourcentage-jquery"> 30 % </p>
            </div>
        
        </div>

        <script>

        $('.couleur-progression8').css("display","none");

        var $document = $( document );


        $document.ready( function () {

            $(window).scroll(
            function() {
            if ($(window).scrollTop() > 2750) 
            {
                $('.couleur-progression8').css("display","block")
                $('.pourcentage-jquery').css("display","block")
            } 
        });
        });
        </script>
        
        
        <div class="espace-bot"> 


        </div>

        
        
<?php
require 'inc/footer.php'
?>