<?php
require 'inc/header.php';
?>
<div class="background-veille">

    <h3 class ="titre-javascript"> Thème choisi : Les failles en Java Script </h3>

    

    <div class="barre-presentation-veille">

    </div>

        <p class="text-presentation-js">
            
            J'ai choisi pour ma veille les failles avec java script car java script plus communément appelé "js" , est le langage de 
            programmation le plus populaire , selon RedMonk il y'aurait 11 millions de développeurs qui l'utiliserait activement.
            Sa popularité n'empêche cependant pas les failles de sécurité et c'est cela que j'aborderais dans ma veille. 
        
        </p>

        <a  class="boutton-date">Trier par date</a>
        <a  class="boutton-pertinence">Trier par pertinence</a>
        

    
    <?php
        
        try
{
	// On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=monsite;charset=utf8', 'moi', 'moi');
    echo '';
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// On récupère tout le contenu de la table article
$reponse = $bdd->query('SELECT * FROM article');

// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{
    ?>

        
         

        <img class="images-bdd" alt="images bdd"  src= <?php echo $donnees['image']; ?> >

        <h2 class="nom-article-bdd"> Nom article : <?php echo $donnees['nom-article']; ?> </h2>
        
        <p class="resume-article"> <?php echo $donnees['resume']; ?>  </p>

        <p class="source-article-bdd"> Source : <?php echo $donnees['source']; ?> </p>

        <p class="date-article-bdd"> Date : <?php echo $donnees['date']; ?> </p>
            
        <p class="lien-article-bdd"> Lien : <?php echo $donnees['lien']; ?> </p>
        


    <?php
}

    ?>

        <?php
        
        $reponse = $bdd->query('SELECT * FROM article ORDER BY pertinence');
        
        while ($donnees = $reponse->fetch())
        {
            ?>
            
            <img class="images-bdd1" alt="images bdd"  src= <?php echo $donnees['image']; ?> >

            <h2 class="nom-article-bdd1"> Nom article : <?php echo $donnees['nom-article']; ?> </h2>
        
            <p class="resume-article"> <?php echo $donnees['resume']; ?>  </p>

            <p class="source-article-bdd1"> Source : <?php echo $donnees['source']; ?> </p>

            <p class="date-article-bdd1"> Date : <?php echo $donnees['date']; ?> </p>
            
            <p class="lien-article-bdd1"> Lien : <?php echo $donnees['lien']; ?> </p>
            
            <?php
        }
        ?>
        

        

        <script>
            
            $(document).ready(function() 
            {
                $('.boutton-pertinence').click(function() {
                $('.images-bdd').css("display","none")
                $('.nom-article-bdd').css("display","none")
                $('.source-article-bdd').css("display","none")
                $('.date-article-bdd').css("display","none")
                $('.lien-article-bdd').css("display","none")
                $('.images-bdd2').css("display","none")
                $('.nom-article-bdd2').css("display","none")
                $('.source-article-bdd2').css("display","none")
                $('.date-article-bdd2').css("display","none")
                $('.lien-article-bdd2').css("display","none")
                $('.images-bdd1').css("display","block")
                $('.nom-article-bdd1').css("display","block")
                $('.source-article-bdd1').css("display","block")
                $('.date-article-bdd1').css("display","block")
                $('.lien-article-bdd1').css("display","block")
            });
            });
          
          </script>


<?php
        
        $reponse = $bdd->query('SELECT * FROM article ORDER BY date ');
        
        while ($donnees = $reponse->fetch())
        {
            ?>
            
            <img class="images-bdd2" alt="images bdd"  src= <?php echo $donnees['image']; ?> >

            <h2 class="nom-article-bdd2"> Nom article : <?php echo $donnees['nom-article']; ?> </h2>
        
            <p class="resume-article"> <?php echo $donnees['resume']; ?>  </p>

            <p class="source-article-bdd2"> Source : <?php echo $donnees['source']; ?> </p>

            <p class="date-article-bdd2"> Date : <?php echo $donnees['date']; ?> </p>
            
            <p class="lien-article-bdd2"> Lien : <?php echo $donnees['lien']; ?> </p>
            
            <?php
        }
        ?>
        

        

        <script>
            
            $(document).ready(function() 
            {
                $('.boutton-date').click(function() {
                $('.images-bdd').css("display","none")
                $('.nom-article-bdd').css("display","none")
                $('.source-article-bdd').css("display","none")
                $('.date-article-bdd').css("display","none")
                $('.lien-article-bdd').css("display","none")
                $('.images-bdd1').css("display","none")
                $('.nom-article-bdd1').css("display","none")
                $('.source-article-bdd1').css("display","none")
                $('.date-article-bdd1').css("display","none")
                $('.lien-article-bdd1').css("display","none")
                $('.images-bdd2').css("display","block")
                $('.nom-article-bdd2').css("display","block")
                $('.source-article-bdd2').css("display","block")
                $('.date-article-bdd2').css("display","block")
                $('.lien-article-bdd2').css("display","block")
            });
            });
          
          </script>


        <?php

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>


        </div>

<?php
require 'inc/footer.php'
?>